package com.example.td3_list_view2;

public class JeuVideo {

    private String name;
    private String price;


    public JeuVideo(String n,String p)
    {
        name = n;

        price = p;

    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(String price) {
        this.price = price;
    }

}
