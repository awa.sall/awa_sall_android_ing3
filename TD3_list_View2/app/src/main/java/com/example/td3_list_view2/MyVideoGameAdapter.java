package com.example.td3_list_view2;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class MyVideoGameAdapter extends  RecyclerView.Adapter<MyVideoGamesViewHolder>
{
    private List<JeuVideo> mesJeux;
    public MyVideoGameAdapter(List<JeuVideo> mesJeux) {
        this.mesJeux = mesJeux;
    }
    @NonNull
    @Override
    //fonction de création du layout .xml
    public MyVideoGamesViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_jeu_video,
                parent, false);
        return new MyVideoGamesViewHolder(view);
    }

    //exécuter lors du reclyclage d'une cellule (quand elle redevient visible
    //on récupére sa position
    //on mets à jour la cellule avec display
    @Override
    public void onBindViewHolder(@NonNull MyVideoGamesViewHolder holder, int position) {
        holder.display(mesJeux.get(position));

    }

    //connaitre le nombre de view holder dans la recycler view
    @Override
    public int getItemCount() {
        return mesJeux.size();
    }
}
