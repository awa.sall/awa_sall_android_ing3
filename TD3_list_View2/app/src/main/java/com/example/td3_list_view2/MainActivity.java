package com.example.td3_list_view2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private TextView price;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //CREER ICI UNE LISTE DE JEUX VIDEO NOMMEE mesJeux ET REMPLISSEZ LA DE JeuxVideo

        JeuVideo z;
        z = new JeuVideo("Zelda","25");
        JeuVideo c;
        c = new JeuVideo("Call of Duty","50");
        JeuVideo s;
        s = new JeuVideo("Sonic","30");
        List<JeuVideo> mesJeux= new ArrayList<JeuVideo>();
        mesJeux.add(z);
        mesJeux.add(c);
        mesJeux.add(s);

        //trouver la list de vue créer
        RecyclerView myRecyclerView = findViewById(R.id.myRecyclerView);

        //disposition de la list de vue : vertical or horizontal
        //pour grille:"new GridLayoutManager(getApplication(),3));
        //pour vertical:"getApplicationContext(), LinearLayoutManager.HORIZONTAL, false)"
        myRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

        //mis a jour par l'adapter qui affiche les différentes vues si possible
        // (l'utilistaur à scroller ou pas)
        myRecyclerView.setAdapter( new MyVideoGameAdapter(mesJeux));

    }
}
