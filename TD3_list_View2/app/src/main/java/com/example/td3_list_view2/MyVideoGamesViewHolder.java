package com.example.td3_list_view2;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MyVideoGamesViewHolder extends RecyclerView.ViewHolder {

    private TextView mNameTV;
    private TextView mPriceTV;
    public MyVideoGamesViewHolder(@NonNull View itemView) {
        super(itemView);
        //Recyclerviw se construit sur une vue ou on recherche les cellules
        mNameTV = itemView.findViewById(R.id.name);
        mPriceTV = itemView.findViewById(R.id.price);
    }

    //fonction display :affichage nom et prix dans le view holder
    void display(JeuVideo jeuVideo){
        mNameTV.setText(jeuVideo.getName());
        mPriceTV.setText(jeuVideo.getPrice() + "$");
    }

    //

}
