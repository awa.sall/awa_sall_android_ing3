package com.example.newlist;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class NewsActivity extends AppCompatActivity {

    //logout->logACT
    //details->detACT
    //back->ec-acc
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
