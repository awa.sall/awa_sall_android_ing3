package com.example.newlist;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.newlist.R;

public class DetailsActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = "NewsList" ;

    //ok->news activity
    //retour->News Activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        Button button_det = findViewById(R.id.butt_ok_details);
        button_det.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.butt_ok_details:
                intent = new Intent(this, com.example.newlist.NewsActivity.class);
                startActivity(intent);
                break;
            case R.id.butt_login:
                intent = new Intent(this, com.example.newlist.LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.butt_logout:
                intent = new Intent(this, com.example.newlist.LoginActivity.class);
                startActivity(intent);
                break;
            case R.id.butt_details:
                intent = new Intent(this, DetailsActivity.class);
                startActivity(intent);
                break;

        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        //Finir l'activité ici
        finish();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "terminaison de details_activity " + getLocalClassName());

    }
}

