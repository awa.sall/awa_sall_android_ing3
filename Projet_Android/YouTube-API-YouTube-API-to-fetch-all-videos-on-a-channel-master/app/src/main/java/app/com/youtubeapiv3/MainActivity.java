package app.com.youtubeapiv3;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

import app.com.youtubeapiv3.fragments.ChannelFragment;

/**
 * Activitée de menu
 */
public class MainActivity extends AppCompatActivity implements View.OnClickListener {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Instanciation des image clickable insérer dans des card view
        CardView button_det = findViewById(R.id.motivationId);
        button_det.setOnClickListener(this);
        CardView button_det_inspi = findViewById(R.id.InspiId);
        button_det_inspi.setOnClickListener(this);
        CardView button_det_Med = findViewById(R.id.MeditId);
        button_det_Med.setOnClickListener(this);
        CardView button_det_work = findViewById(R.id.WorkId);
        button_det_work.setOnClickListener(this);
    }
    //methode onclick qui étudie chaque cas
    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.motivationId:

               intent = new Intent(this, app.com.youtubeapiv3.HomeActivity.class);
                startActivity(intent);
                break;
            case R.id.InspiId:
                intent = new Intent(this, app.com.youtubeapiv3.HomeActivityInspiration.class);
                startActivity(intent);
                break;
            case R.id.MeditId:
                intent = new Intent(this, app.com.youtubeapiv3.HomeActivityMeditation.class);
                startActivity(intent);
                break;
            case R.id.WorkId:
                intent = new Intent(this, app.com.youtubeapiv3.HomeActivityWorkOut.class);
                startActivity(intent);
                break;

        }
    }


}
