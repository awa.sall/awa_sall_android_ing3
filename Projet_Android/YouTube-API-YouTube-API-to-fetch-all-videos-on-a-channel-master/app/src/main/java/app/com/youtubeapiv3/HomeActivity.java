package app.com.youtubeapiv3;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import app.com.youtubeapiv3.fragments.ChannelFragment;

public class HomeActivity extends FragmentActivity implements View.OnClickListener {

        protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        //Id de la chennel choisi pour la categorie Diet
        String channel_id = "UCj0V0aG4LcdHmdPJ7aTtSCQ";

        //Instanciation personnalisé d'un frament avec le channel_id
        ChannelFragment myf = ChannelFragment.newInstance(channel_id);
        final android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();

        //on remplace la vue actuelle par le fragment pour permettre son chargement automatique
        transaction.replace(R.id.mainFrame,myf);
        transaction.addToBackStack(null);
        transaction.commit();
        //Déclaration du button de retour au menu
        Button button_det_work = findViewById(R.id.backtomenu);
        button_det_work.setOnClickListener(this);

    }
//Boutton pour revenir au menu
    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
        case R.id.backtomenu:
        intent = new Intent(this, app.com.youtubeapiv3.MainActivity.class);
        startActivity(intent);
        break;}
    }
}

