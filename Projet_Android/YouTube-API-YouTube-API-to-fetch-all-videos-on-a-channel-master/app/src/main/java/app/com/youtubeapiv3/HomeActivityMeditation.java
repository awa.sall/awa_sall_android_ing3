package app.com.youtubeapiv3;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;
import app.com.youtubeapiv3.fragments.ChannelFragment;

/**Pareil que dans home activity**/
public class HomeActivityMeditation extends FragmentActivity implements View.OnClickListener{
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        String channel_id = "UCt_3fRzjarr5LxXK9ELstZw";
        ChannelFragment myf = ChannelFragment.newInstance(channel_id);
        final android.support.v4.app.FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.mainFrame,myf);
        transaction.addToBackStack(null);
        Button button_det_work = findViewById(R.id.backtomenu);
        button_det_work.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()) {
            case R.id.backtomenu:
                intent = new Intent(this, app.com.youtubeapiv3.MainActivity.class);
                startActivity(intent);
                break;}
    }
}