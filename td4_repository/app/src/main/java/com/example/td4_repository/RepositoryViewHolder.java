package com.example.td4_repository;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class RepositoryViewHolder extends RecyclerView.ViewHolder {
    private TextView mIdTV;
    private TextView mNameTV;
    public RepositoryViewHolder(@NonNull View itemView) {
        super(itemView);
        //Recyclerviw se construit sur une vue ou on recherche les cellules
        mNameTV = itemView.findViewById(R.id.name);
        mIdTV = itemView.findViewById(R.id.id);
    }

    //fonction display :affichage nom et prix dans le view holder
    void display(RepoList item_repo){
        mIdTV.setText(Integer.toString(item_repo.getId()) );
        mNameTV.setText(item_repo.getName());
    }

}
