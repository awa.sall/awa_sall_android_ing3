package com.example.td4_repository;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;


public class RepositoryAdapter extends  RecyclerView.Adapter<RepositoryViewHolder>
{
    private List<RepoList> mesRepos;
    public RepositoryAdapter(List<RepoList> mesRepo) {
        this.mesRepos = mesRepo;
    }

    @NonNull
    @Override
    //fonction de création du layout .xml
    public  RepositoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repo,
                parent, false);
        return new RepositoryViewHolder(view);
    }

    //exécuter lors du reclyclage d'une cellule (quand elle redevient visible
    //on récupére sa position
    //on mets à jour la cellule avec display
    @Override
    public void onBindViewHolder(@NonNull RepositoryViewHolder holder, int position) {
        holder.display(mesRepos.get(position));

    }

    //connaitre le nombre de view holder dans la recycler view
    @Override
    public int getItemCount() {
        return mesRepos.size();
    }

}
