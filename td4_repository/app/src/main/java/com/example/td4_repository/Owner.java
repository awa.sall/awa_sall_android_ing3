package com.example.td4_repository;

public class Owner {

public String login;
public Integer id;
public String nodeId;
public String avatarUrl;
public String gravatarId;
public String url;
public String htmlUrl;
public String followersUrl;
public String followingUrl;
public String gistsUrl;
public String starredUrl;
public String subscriptionsUrl;
public String organizationsUrl;
public String reposUrl;
public String eventsUrl;
public String receivedEventsUrl;
public String type;
public Boolean siteAdmin;

    public String getLogin() {
        return login;
    }

    public Integer getId() {
        return id;
    }

    public String getNodeId() {
        return nodeId;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getGravatarId() {
        return gravatarId;
    }

    public String getUrl() {
        return url;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public String getFollowersUrl() {
        return followersUrl;
    }

    public String getFollowingUrl() {
        return followingUrl;
    }

    public String getGistsUrl() {
        return gistsUrl;
    }

    public String getStarredUrl() {
        return starredUrl;
    }

    public String getSubscriptionsUrl() {
        return subscriptionsUrl;
    }

    public String getOrganizationsUrl() {
        return organizationsUrl;
    }

    public String getReposUrl() {
        return reposUrl;
    }

    public String getEventsUrl() {
        return eventsUrl;
    }

    public String getReceivedEventsUrl() {
        return receivedEventsUrl;
    }

    public String getType() {
        return type;
    }

    public Boolean getSiteAdmin() {
        return siteAdmin;
    }
}