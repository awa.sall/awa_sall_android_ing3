package com.example.td4_repository;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //instanciation de la méthode builder et rétroprofit

        Retrofit.Builder builder = new Retrofit.Builder()
                .baseUrl("https://api.github.com")
                .addConverterFactory(GsonConverterFactory.create());

        //instanciatiation d'un objet retrofit avec le builder
        Retrofit retrofit = builder.build();
        GitHubClient client = retrofit.create(GitHubClient.class);
        Call<List<RepoList>> call = client.UserRepositories("JakeWharton");

        //
        call.enqueue(new Callback<List<RepoList>>() {
            @Override
            public void onResponse(Call<List<RepoList>> call, Response<List<RepoList>>
                    response) {

                List<RepoList> mesRepos = response.body();
                int s;
                s = mesRepos.size();
                //trouver la list de vue créer
                RecyclerView myRecyclerView = findViewById(R.id.myRecyclerView);

                //disposition de la list de vue : vertical or horizontal
                //pour grille:"new GridLayoutManager(getApplication(),3));
                //pour vertical:"getApplicationContext(), LinearLayoutManager.HORIZONTAL, false)"
                myRecyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

                //mis a jour par l'adapter qui affiche les différentes vues si possible
                // (l'utilistaur à scroller ou pas)
                RepositoryAdapter adpt =new RepositoryAdapter(mesRepos);
                adpt.notifyDataSetChanged();
                myRecyclerView.setAdapter( adpt);
            }

            @Override
            public void onFailure(Call<List<RepoList>> call, Throwable t)
            {
                Toast.makeText(MainActivity.this, "Error...!!!", Toast.LENGTH_SHORT).show();
            }
        });






    }


}
